library(tidyverse)

setwd("/nas/longleaf/home/jmvalone/Neuroanalytics/")

##Making a histogram plot for visualizing minor allele frequencies for all SNPs


##Get all my filenames of interest
fped <- 'hapmap1_nomissing_AF.ped';
fmap <- 'hapmap1_nomissing_AF.map';

##Read in the ped file
ped <- read.table(fped);
##Transform this ped file into readable SNP data
##First remove the first six columns which are not SNP info
tmpSNPs <- ped[,c(7:ncol(ped))];
##To get the total number of SNPs, since each SNP is two columns we can do:
nSNPs <- ncol(tmpSNPs)/2;
##Get the total number of individuals in the study
ndonors <- nrow(ped);
##Display this as output to the user
cat('The total number of SNPs is: ',nSNPs,'\n');
cat('The total number of donors is: ',ndonors,'\n');

##Reformat the ped file to have
##0 0 = NA
##1 1 = homozygous for allele 1 = 0
##1 2 = heterozygous = 1
##2 2 = homozygous for allele 2 = 2
##Make an output matrix
SNPs <- matrix(NA,nrow=ndonors,ncol=nSNPs);
##Loop over each pair of columns in the tmpSNPs matrix
##I want this to get a series of numbers that are like this
##1,2  3,4  5,6 ...
##This is equivalent to (2*i-1,2*i)
for (i in 1:nSNPs) {
  
  ##Make an empty vector containing what the recoded SNP values will be
  snprecode <- matrix(NA,nrow=ndonors,ncol=1);
  
  ##Get the two columns representing one SNP
  onesnp <- tmpSNPs[,c(2*i-1,2*i)];
  
  ##Classify each SNP into 5 categories
  NAind <- which(onesnp[,1]==0 & onesnp[,2]==0);
  hom1ind <- which(onesnp[,1]==1 & onesnp[,2]==1);
  hetind <- which((onesnp[,1]==1 & onesnp[,2]==2) | (onesnp[,1]==2 & onesnp[,2]==1));
  hom2ind <- which(onesnp[,1]==2 & onesnp[,2]==2);
  
  ##Recode the SNPs according to their allelic status
  snprecode[NAind] <- NA;
  snprecode[hom1ind] <- 0;
  snprecode[hetind] <- 1;
  snprecode[hom2ind] <- 2;
  
  ##Save all of these SNPs in a matrix
  SNPs[,i] <- snprecode;
  
}


##Use the map file to label which SNP is being referred to in the SNPs file
map <- read.table(fmap);
##Make sure the number of SNPs in the map file equals the number of SNPs in the ped file
if(nrow(map)!=nSNPs) {
  cat('Something is wrong! The number of SNPs in the map file is not equal to the number of SNPs in the ped file','\n');
}
##I already know these SNPs are in the same order
##So I will assign the SNP names to the columns
colnames(SNPs) <- map$V2;
##Also now assign each row to the subject
rownames(SNPs) <- ped[,1];


#Determine allele frequency
alleles <- colSums(SNPs,na.rm=TRUE);
totalN <- 2*colSums(!is.na(SNPs));
freq <- alleles/totalN;

#Subtract all AFs > 0.5 from 1 to get MAFs
for (i in 1:length(freq)) {
  j <- freq[i]
  if (j > 0.5) {
    freq[i] <- 1-j 
    }
}

#Plotting MAF histogram 

freq <- as.data.frame(freq)

pdf('genohisto.pdf');

#Plotting data
ggplot(data = freq) + 
  geom_histogram(mapping = aes(x = freq), binwidth = 0.02) +
  xlab("Minor Allele Frequency") + ylab("# of SNPs") +
  ggtitle("Distribution of Minor Allele Frequencies") +
  theme(plot.title = element_text(hjust = 0.5)) -> J
print(J)

dev.off()