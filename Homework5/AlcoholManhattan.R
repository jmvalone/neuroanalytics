#Setting the table
setwd("/nas/longleaf/home/jmvalone/Neuroanalytics")

library(tidyverse)

#Loading in dataset
OH <- read_tsv("/nas/longleaf/home/jmvalone/Neuroanalytics/20414.gwas.imputed_v3.both_sexes.tsv.gz")


#Arrange by pvalue to identify lowest pval
pvalarranged <- arrange(OH, pval) #Find minimum p-value

#Insert something here to output lowest p-value

#Split column 1 to different variables
splitinfo <- str_split(pvalarranged$variant, ":", simplify = TRUE)

#Mutate to add split up column
newOH <- mutate(pvalarranged, Chromosome = splitinfo[,1],Chr_Position = splitinfo[,2] )


#Filter to only desired chromosome info and p-value
filtered <- filter(newOH, Chromosome == 4)
filtered2 <- filter(filtered, pval <= 1.00000e-04)

#Make Chromosome position a numeric
filtered2$Chr_Position <- as.numeric(filtered2$Chr_Position)

#Plot -log10 pvalue against chromosome position
pdf('AlcoholManhattan.pdf');

ggplot(data = filtered2, aes(x = Chr_Position, y = -log10(pval))) +
  geom_point() +
  geom_hline(yintercept = -log10(5e-8), linetype = "dashed") -> J
print(J)

dev.off()
