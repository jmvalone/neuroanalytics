Folder for Homework 5 scripts: 3 total

Phenohisto interpretation: The phenotype values follow a normal distribution with a mean of roughly 6.5 and a notable trough at the mean value.

Genohisto interpretation: The genotype MAF values are evenly distributed with a small increase at the lowest values and a more notable decrease at values close to 0.5

AlcoholManhattan: Some significant signal early in Chromosome 4 and an area entire cluster of significant values towards the end