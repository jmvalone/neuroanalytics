#!/bin/bash

header="data" #Setting "data" to header variable

for i in `seq 1 100`; #Setting up the outer loop to make 100 different donor files
do
  if [ ${i} -lt 10 ]; #Using if statement to add leading zeroes based on number being less than 10 
    then i=00${i};
  elif [ ${i} -lt 100 ]; #Using elif to add leading zeroes based on number being less than 100
    then i=0${i}; 
    fi; #Closing if statement
  for j in `seq 1 10`; #Setting up inner loop to make 10 time points for each donor
  do
    if [ ${j} -lt 10 ]; #Using if statement to add leading zeros based on number being less than 10
      then j=00${j};
    else j=0${j}; #Adding 1 leading zero to make 010
      fi; #Close if statement
echo "Creating file: donor${i}_${j}tp.txt"; #Prompt user what files being created
    (echo $header
  echo $RANDOM
  echo $RANDOM 
  echo $RANDOM
  echo $RANDOM 
  echo $RANDOM) >> "donor${i}_tp${j}.txt" #Echoing the desired input into the file just created, data header followed by 5 lines of random numbers 
  done
done #Closing out both for loops