#!/bin/bash

#Putting all files for each donor in individual directories
for p in `seq 1 100` #Making a sequence of 1 to 100 for each donor
do
  if [ ${p} -lt 10 ]; #Using if statement to add leading zeroes based on number being less than 10 
    then p=00${p};
  elif [ ${p} -lt 100 ]; #Using elif to add leading zeroes based on number being less than 100
    then p=0${p}; 
  else p=${p}
    fi; #Closing if statement
  mkdir -p "Donor${p}" #Make a directory for Donor 1 to 100
  mv donor${p}_tp* Donor${p} #Move all 10 tp files into donors directory
done

#Putting all of each donor's files into a directory called fakedata
mkdir fakedata #Making fake directory
mv Donor* fakedata/ #Moving all donors into single folder called fakedata


