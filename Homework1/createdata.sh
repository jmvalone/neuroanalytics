#!/bin/bash

header="data" #Setting "data" to header variable

for i in `seq 1 50`; #Setting up the outer loop to make 50 different donor files
do
  for j in `seq 1 10`; #Setting up inner loop to make 10 time points for each donor
  do
    echo "Creating file: donor${i}_${j}tp.txt"; #Prompt user what files being created
    touch donor${i}_tp${j}.txt;  #Create the file with correc donor ID and timepoint
    (echo $header
  echo $RANDOM
  echo $RANDOM 
  echo $RANDOM
  echo $RANDOM 
  echo $RANDOM) >> "donor${i}_tp${j}.txt" #Echoing the desired input into the file just created, data header followed by 5 lines of random numbers 
  done
done #Closing out both for loops


    

  
