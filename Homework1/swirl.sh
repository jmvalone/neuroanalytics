#!/bin/bash

#Original code
echo -ne '/';  #-n: do not output trailing newline; -e: enable interpretation of backslash escape characters
sleep 1;
echo -ne '\b-';  #same as above for new with addition \b: backspace to post only '-' within the same line
sleep 1;
echo -ne '\b\\' #same inputs as previous line with a \ after \b as an escape character to enable posting '\' next
sleep 1;
echo -ne '\b|'; #repeating previous commands to post '|' , note the lack of a '\' after b as the escape charcter is not needed here
sleep 1;
echo -ne '\b/'; #repeating previous commands to post '/' , note the lack of a second '\' for same reason as above
sleep 1;
echo -ne '\b-'; #repeat of line 5
sleep 1;
echo -ne '\b\\'; #repeat of line 7
sleep 1;
echo -ne '\b|'; #repeat of line 9 BUT due to a lack of sleep command following this the '|' is not presented for any noticeable period of time before line 18 runs
echo -ne "\b \n"; #this line deletes the final character, technically a '|' but the last shown i see is '\', before using the \n option to move to a new (blank) line

#Altered Spin direction counter clockwise by reversing order of above lines of code
echo -ne '/';
sleep 1;
echo -ne '\b|';
sleep 1;
echo -ne '\b\\'
sleep 1;
echo -ne '\b-';
sleep 1;
echo -ne '\b/';
sleep 1;
echo -ne '\b|';
sleep 1;
echo -ne '\b\\'
sleep 1;
echo -ne '\b-';
sleep 1;
echo -ne '\b/';
sleep 1;
echo -ne "\b \n";

#Altered Spin direction counter clockwise and speed to 0.2 seconds sleep
echo -ne '/';
sleep 0.2; #Note changed sleep time to 0.2 seconds here and following
echo -ne '\b|';
sleep 0.2;
echo -ne '\b\\'
sleep 0.2;
echo -ne '\b-';
sleep 0.2;
echo -ne '\b/';
sleep 0.2;
echo -ne '\b|';
sleep 0.2;
echo -ne '\b\\'
sleep 0.2;
echo -ne '\b-';
sleep 0.2;
echo -ne '\b/';
sleep 0.2;
echo -ne "\b \n";

#Altered Spin direction counter clockwise, speed to 0.2 seconds sleep, and 10 rotations
for i in `seq 1 10` #for loop sets up this sequence to repeat 10 times. i acts as stand in for whatever repitition within sequence is occuring
do 
  echo "Starting spin ${i} Counter Clockwise:" #Make a statement to tell user which spin in the sequence is occuring
  echo -ne '/'; #same format as previous portion of script just within a loop
  sleep 0.2;
  echo -ne '\b|';
  sleep 0.2;
  echo -ne '\b\\'
  sleep 0.2;
  echo -ne '\b-';
  sleep 0.2;
  echo -ne '\b/';
  sleep 0.2;
  echo -ne '\b|';
  sleep 0.2;
  echo -ne '\b\\'
  sleep 0.2;
  echo -ne '\b-';
  sleep 0.2;
  echo -ne '\b/';
  sleep 0.2;
  echo -ne "\b \n";
done #Closing loop

#Altered Spin direction counter clockwise and then clockwise, speed to 0.2 seconds sleep, and 10 repititions (20 rotations)
for i in `seq 1 10`
do 
  echo "Starting spin ${i} Counter Clockwise:" #Start of counter clockwise spin
  echo -ne '/';
  sleep 0.2;
  echo -ne '\b|';
  sleep 0.2;
  echo -ne '\b\\'
  sleep 0.2;
  echo -ne '\b-';
  sleep 0.2;
  echo -ne '\b/';
  sleep 0.2;
  echo -ne '\b|';
  sleep 0.2;
  echo -ne '\b\\'
  sleep 0.2;
  echo -ne '\b-';
  sleep 0.2;
  echo -ne '\b/';
  sleep 0.2;
  echo -ne "\b"; #End of counter clockwise spin
  echo -ne "Reverse!"; #Post "Reverse!" in line
  sleep 0.5;
  echo -ne "\b\b\b\b\b\b\b\b        "; #Make line blank for a moment to cause flashing effect between "Reverse!" outputs
  sleep 0.5; #Longer sleep time to increase flash effect prominence
  echo -ne "\b\b\b\b\b\b\b\bReverse!"; #Post second "Reverse!" in same line
  sleep 0.5;
  echo -ne "\b\b\b\b\b\b\b\b        "; #Again, blank link for flashing effect
  sleep 0.5;
  echo -e "\b\b\b\b\b\b\b\bStarting spin ${i} Clockwise:" #Prompt telling user clockwise spin is beginning with i representing repitition 
  echo -ne '/';  
  sleep 0.2;
  echo -ne '\b-';  
  sleep 0.2;
  echo -ne '\b\\' 
  sleep 0.2;
  echo -ne '\b|'; 
  sleep 0.2;
  echo -ne '\b/'; 
  sleep 0.2;
  echo -ne '\b-'; 
  sleep 0.2;
  echo -ne '\b\\'; 
  sleep 0.2;
  echo -ne '\b|'; 
  sleep 0.2
  echo -ne '\b/';
  sleep 0.2
  echo -ne "\b \n"; 
  sleep 0.2
done #Closing loop