#!/bin/bash

#Putting all files for each donor in individual directories
for p in `seq 1 50` #Making a sequence of 1 to 50 for each donor
do
  mkdir -p "Donor${p}" #Make a directory for Donor 1 to 50
  mv donor${p}_tp* Donor${p} #Move all 10 tp files into donors directory
done

#Putting all of each donor's files into a directory called fakedata
mkdir fakedata #Making fake directory
mv Donor* fakedata/ #Moving all donors into single folder called fakedata

#Making files read only
files=./fakedata/*/*
for file in $files
do
  test -f "$file" &&
  chmod a-wx "$file"
done
