#!/bin/bash

##Script to print hapmap1.ped lines 1 at a time with 1 second in between display of each line
#Made edits after reviewing assignment in class 1/23 to cleanup the line count output for personal reference but did not edit the cat/while/read loop portion.
#If original is preferred for grading purposes let me know :)
    
FILE=hapmap1.ped; 
VAR1=$(wc -l $FILE); #Setting FILE as line count of hapmap1.ped file
VAR2=$(basename $VAR1 $FILE1); #setting hapmap1 line count as VAR2

echo "Determining number of lines for "$FILE" file:" #Prompt user number of lines in file are being determined
sleep 2 #sleep 2 seconds

echo "$VAR2 lines in $FILE" #echo line count to user
sleep 2 #sleep 2 seconds
   

cat hapmap1.ped | while read line; #read hapmap1.ped file piped into while command to continue until no more lines to read
do
  echo "$line" #output lines 1 at a time
  sleep 1 #sleep 1 second between line outputs
done #close loop
